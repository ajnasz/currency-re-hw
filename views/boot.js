(function () {
	var domLoaded = false;
	document.addEventListener('DOMContentLoaded', function () {
		domLoaded = true;
	});

	require(['app'], function (app) {
		if (domLoaded) {
			app();
		} else {
			document.addEventListener('DOMContentLoaded', function () {
				app();
			});
		}
	});
}()); 
