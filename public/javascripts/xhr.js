define(function () {
	function XHRRequest (config) {
		let xhr = new XMLHttpRequest();	
		xhr.open(config.method || 'GET', config.url);
		xhr.addEventListener('readystatechange', function () {
			let DONE = xhr.DONE || 4;
			if (xhr.readyState === DONE) {
				config.success(xhr.responseText);
			}
		}, false);

		return xhr;
	}

	return XHRRequest;
});
