define (['vendor/lodash', 'vendor/events'], function (_, events) {
	function addListener (details, view) {
		view.container.querySelector(details.selector)
			.addEventListener(details.event, _.bind(view[details.callback], view), false);
	}

	var view = {
		configuration: null,

		initListeners: function () {
			var events = _.get(this.configuration, 'domEvents');
			_.forEach(events, _.bind(function (details) {
				addListener(details, this);
			}, this));
		},

		/**
		 * @param {Object} config
		 *
		 * configuration: {
		 * 	containerSelector: '',
		 * 	container: null,
		 * 	events: [
		 * 		{
		 * 			selector: null,
		 * 			event: 'click',
		 * 			callback: 'callbackName'
		 * 		}
		 * 	]
		 * },
		 */
		init: function (config) {
			this.configuration = _.merge({}, this.configuration || {}, config);
			_.set(this, 'container', document.querySelector(_.get(this.configuration, 'containerSelector')));

			this.events = events();

			this.initListeners();
		}
	};

	return view;
});
