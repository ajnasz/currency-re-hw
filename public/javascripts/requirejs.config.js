/* global requirejs */
requirejs.config({
	baseUrl: '/javascripts',
	paths: {
		d3: 'vendor/d3'
	}
});
