define(['vendor/lodash', 'vendor/xhr', 'controller'], function (_, xhr, controller) {
	var currencyConverterController = _.extend({}, controller, {
		onXHRError: function (err, response) {
			this.view.onError(err, response);
		},

		showRates: function (data) {
			this.view.showRates(data);
		},

		onCurrencyResponse: function (err, response) {
			if (err) {
				this.onXHRError(err, response);
			} else {
				this.showRates(response);
			}
		},

		onSubmit: function (params) {
			xhr.get({
				url: '/calculate',
				data: params,
				headers: {
					Accept: 'application/json'
				}
			}, _.bind(this.onCurrencyResponse, this));
		},
		viewEvents: [
			{event: 'submit', callback: 'onSubmit'}
		]
	});

	return currencyConverterController;
});
