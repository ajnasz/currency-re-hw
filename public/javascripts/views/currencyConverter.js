define([
	'vendor/lodash',
	'widgets/notification',
	'text!templates/currencyConverter.html',
	'view'
], function (_, notification, template, view) {
	var tpl = _.template(template);

	var currencyConverterView = _.extend({}, view, {
		configuration: {
			containerSelector: '#CurrencyConverter',
			domEvents: [
				{
					selector: '.js-form',
					event: 'submit',
					callback: 'onFormSubmit'
				}
			]
		},

		onFormSubmit: function (e) {
			e.preventDefault();
			notification.hide();
			var form = this.container.querySelector('.js-form');

			var currencyFrom = form.querySelector('.js-currency-from').value;
			var currencyTo = form.querySelector('.js-currency-to').value;
			var amount = form.querySelector('.js-amount').value;

			var params = {
				'amount': amount,
				'currencyFrom': currencyFrom,
				'currencyTo': currencyTo
			};

			this.events.emit('submit', params);
		},

		showRates: function (data) {
			var result = this.container.querySelector('.js-result');

			while (result.firstChild) {
				result.removeChild(result.firstChild);
			}

			result.insertAdjacentHTML('afterbegin', tpl(data));
		},

		onError: function (err, response) {
			console.log(err, response);
			
			notification.error({
				messages: [response.message || 'Unknown error']
			});
		}
	});

	return currencyConverterView;
});
