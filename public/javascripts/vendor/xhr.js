/*global define*/
define(function () {
	function addContentTypeHeader (params) {
		params.headers = params.headers || {};
		params.headers['Content-Type'] = 'application/json';
	}

	function objectToQueryString (data) {
		return Object.keys(data).map(function (key) {
			return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
		}).join('&');
	}

	function createGetUrl (url, data) {
		return url + '?' + objectToQueryString(data);
	}

	function sendXhrRequest (params, cb) {
		var method, request;

		params = params || {};

		method = (params.method || 'get').toLowerCase();


		if (method === 'get' && params.data) {
			params.url = createGetUrl(params.url, params.data);
		}

		request = new XMLHttpRequest();
		request.withCredentials = true;

		request.open(method, params.url, true);

		if (params.headers && params.headers) {
			Object.keys(params.headers).forEach(function (header) {
				request.setRequestHeader(header, params.headers[header]);
			});
		}

		request.addEventListener('error', function () {
			var err = new Error('Request Error');
			err.statusCode = request.status;
			err.response = request.response;
			cb(err, null);
		});

		request.addEventListener('readystatechange', function () {
			if (request.readyState === 4) {
				var res = request.responseText,
					contentType;

				contentType = request.getResponseHeader('Content-Type');
				if (contentType && contentType.toLowerCase().indexOf('application/json') > -1) {
					res = JSON.parse(res);
				}

				if (request.status < 300 && request.status >= 200) {
					cb(null, res);
				} else {
					var error = new Error('Request status code error');
					error.statusCode = request.status;
					error.response = res;
					cb(error, res);
				}
			}
		});

		if (params.data) {
			request.send(JSON.stringify(params.data));
		} else {
			request.send();
		}

		return request;
	}


	return {
		request: function xhrRequest (params, method, cb) {
			params.method = method;
			return sendXhrRequest(params, cb);
		},

		post: function xhrPostRequest (params, cb) {
			addContentTypeHeader(params);
			return this.request(params, 'post', cb);
		},

		del: function xhrDeleteRequest (params, cb) {
			return this.request(params, 'delete', cb);
		},

		put: function xhrPutRequest (params, cb) {
			addContentTypeHeader(params);
			return this.request(params, 'put', cb);
		},

		get: function xhrGetRequest (params, cb) {
			return this.request(params, 'get', cb);
		}
	};
});
