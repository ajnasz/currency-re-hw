/* eslint-env jquery, browser */

$(function () {
	$('#CurrencyConverter').on('submit', (e) => {
		e.preventDefault();
		let form = $(e.target);

		let currencyFrom = form.find('[name="currencyFrom"]').val();
		let currencyTo = form.find('[name="currencyTo"]').val();
		let amount = form.find('[name="amount"]').val();

		$.ajax({
			url: '/calculate',

			data: {
				currencyFrom,
				currencyTo,
				amount
			},

			headers: {
				Accept: 'application/json'
			},

			success (res) {
				let exchange = res.exchange.toFixed(2);

				$('#Result').text(`${res.amount} ${res.rateFrom.currency} = ${exchange} ${res.rateTo.currency}`);
			}
		});
	});
});
