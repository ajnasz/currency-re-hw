define(['vendor/lodash'], function (_) {
	function addLitenerToView (controller, view, item) {
		view.events.on(item.event, _.bind(controller[item.callback], controller));
	}

	var controller = {
		configuration: {
			viewEvents: [
				{
					event: 'submit',
					callback: 'onSubmit'
				}
			]
		},

		initListeners: function () {
			var events = _.get(this.configuration, 'viewEvents');

			_.forEach(events, _.bind(function (event) {
				addLitenerToView(this, this.configuration.view, event);
			}, this));
		},

		init: function (config) {
			this.configuration = _.merge({}, this.configuration || {}, config);
			this.view = this.configuration.view; // alias
			this.initListeners();
		}
	};

	return controller;
});
