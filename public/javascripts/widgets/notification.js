define([
	'vendor/bluebird',
	'vendor/lodash',
	'text!templates/notification.html'
], function (Promise, _, template) {
	var tpl = _.template(template);


	function getTrackName (e) {
		e.type.indexOf('animation') === 0 ? e.animationname : e.type;
	}

	function transitionTrack (elem) {
		console.trace();
		return new Promise(resolve => {
			let transitions = new Set();

			function transitionStart (e) {
				if (e.target === elem) {
					transitions.add(getTrackName(e));
				}
			}

			function onTransitionEnd (e) {
				if (e.target !== elem) {
					return;
				}

				transitions.delete(getTrackName(e));

				if (transitions.size === 0) {
					elem.removeEventListener('transitionstart', transitionStart, false);
					elem.removeEventListener('transitionend', onTransitionEnd, false);

					elem.removeEventListener('animationstart', transitionStart, false);
					elem.removeEventListener('animationend', onTransitionEnd, false);
					resolve(elem);
				}
			}

			elem.addEventListener('transitionstart', transitionStart), false;
			elem.addEventListener('transitionend', onTransitionEnd, false);

			elem.addEventListener('animationstart', transitionStart, false);
			elem.addEventListener('animationend', onTransitionEnd, false);
		});
	}

	function hide () {
		var elem = document.querySelector('#Notification');
		var currentClass = elem.className;

		var classListArray = currentClass.split(' ');

		if (classListArray.indexOf('hide') === -1) {
			classListArray.push('hide');
		}

		var promise = transitionTrack(elem);

		elem.className = classListArray.filter(function (className) {
			return className !== 'show';
		}).join(' ');

		return promise;

	}

	function show () {
		var elem = document.querySelector('#Notification');
		var currentClass = elem.className;

		var classListArray = currentClass.split(' ');

		if (classListArray.indexOf('show') === -1) {
			classListArray.push('show');
		}

		var promise = transitionTrack(elem);

		elem.className = classListArray.filter(function (className) {
			return className !== 'hide';
		}).join(' ');

		return promise;

	}

	return {
		error: function (data) {
			var html = tpl(data);
			var noti = document.querySelector('#Notification');

			if (noti) {
				hide().then(function () {
					noti.parentNode.removeChild(noti);
					document.querySelector('body').insertAdjacentHTML('beforeend', html);
				}).then(show);
			} else {
				document.querySelector('body').insertAdjacentHTML('beforeend', html);
				setTimeout(function () {
					show();
				}, 50);
			}
		},

		hide () {
			var noti = document.querySelector('#Notification');

			if (noti) {
				hide().then(function () {
					noti.parentNode.removeChild(noti);
				});
			}
		}
	};
});
