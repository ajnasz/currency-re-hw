define('app', ['d3', 'vendor/rickshaw'], function (d3, Rickshaw) {
	return function () {
		var hs = window.cc.history;

		var data = hs.data.map(function (d) {
			return {
				x: new Date(d.time).getTime() / 1000,
				y: d.rate
			};
		});

		var min = Math.min.apply(Math, hs.data.map(function (h) {
			return h.rate;
		}));
		min *= 0.1;

		var graph = new Rickshaw.Graph({
			element: document.querySelector('#HistoryChart'),
			renderer: 'line',
			min: min,
			series: [{
				color: 'steelblue',
				data: data,
				name: hs.currency
			}]
		});


		new Rickshaw.Graph.Axis.Time({
			graph: graph
		});

		new Rickshaw.Graph.HoverDetail({
			graph: graph
		});

		new Rickshaw.Graph.Axis.Y({
			graph: graph,
			element: document.querySelector('#HistoryChartYAxis')
		});

		graph.render();
	};
});
