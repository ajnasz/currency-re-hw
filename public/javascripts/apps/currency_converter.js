define('app', [
	'vendor/lodash',
	'views/currencyConverter',
	'controllers/currencyConverter'
], function (_, view, controller) {
	return function () {
		var controllerInstance = _.extend({}, controller);
		var viewInstance = _.extend({}, view);

		viewInstance.init();

		controllerInstance.init({
			view: viewInstance
		});
	};
});
