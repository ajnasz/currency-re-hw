var feedparser = require('../src/feedparser');
var test = require('tape');
var _ = require('lodash');

test('exchangeRatesToList returns a promise', function (t) {
	t.plan(1);
	t.ok(feedparser.exchangeRatesToList() instanceof Promise, 'test exchangeRatesToList instance of Promise');
	t.end();
});

test('exchangeRatesToList produces an array', function (t) {
	let obj = require('./pretty.json');
	t.plan(1);

	feedparser.exchangeRatesToList(obj).then(o => {
		t.ok(Array.isArray(o), 'exchangeRatesToList produces array');
		t.end();
	});
});

test('exchangeRatesToList produces an empty array if input is null', function (t) {
	t.plan(1);
	feedparser.exchangeRatesToList(null).then(o => {
		t.ok(Array.isArray(o), 'exchangeRatesToList produces array');
		t.end();
	});
});

test('exchangeRatesToList produces an empty array if input is an empty object', function (t) {
	t.plan(1);
	feedparser.exchangeRatesToList({}).then(o => {
		t.ok(Array.isArray(o), 'exchangeRatesToList produces array');
		t.end();
	});
});

test('exchangeRatesToList produces a one dimensional array', function (t) {
	let obj = require('./pretty.json');

	feedparser.exchangeRatesToList(obj).then(o => {
		o.forEach(i => {
			if (Array.isArray(i)) {
				t.fail('item is an array', i);
			}
		});

		t.ok('None of the items is array');

		t.end();
	});
});

test('exchange.exchangeRatesToList all items should have a time and an exchanges key', function (t) {
	let obj = require('./pretty.json');

	feedparser.exchangeRatesToList(obj).then(o => {
		o.forEach(i => {
			if (!_.has(i, 'time')) {
				t.fail('time field is missing', i);
			}
			if (!_.has(i, 'currency')) {
				t.fail('currency field is missing', i);
			}
			if (!_.has(i, 'rate')) {
				t.fail('rate field is missing', i);
			}
		});

		t.ok('All items has time, currency and rate keys');
		t.end();
	});
});

test('exchange.exchangeRatesToList all items should convert time to date object', function (t) {
	let obj = require('./pretty.json');

	feedparser.exchangeRatesToList(obj).then(o => {
		o.forEach(i => {
			if (!_.get(i, 'time') instanceof Date) {
				t.fail('item is not a Date object', i);
			}
		});
		t.ok('All time field is a Date object');
		t.end();
	});
});

test('exchange.exchangeRatesToList all items should convert rate to a float', function (t) {
	let obj = require('./pretty.json');

	feedparser.exchangeRatesToList(obj).then(o => {
		o.forEach(i => {
			let rate = _.get(i, 'rate');

			if (!isNaN(rate) && rate % 1 === 0) {
				t.fail('rate is not a float', rate);
			}
		});
		t.ok('All rate is a float');
		t.end();
	});
});

