var exchange = require('../src/exchange');
var test = require('tape');

test('exchange.getExchanges returns a promise', function (t) {
	t.plan(1);
	t.ok(exchange.getExchanges() instanceof Promise, 'test exchange.getExchanges instance of Promise');
	t.end();
});
