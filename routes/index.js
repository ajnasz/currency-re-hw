let express = require('express');
let router = new express.Router();
let exchange = require('../src/exchange');
let _ = require('lodash');

function formatError (res, error, status = 500) {
	return res.status(status).format({
		'text/html': function () {
			res.render('error', error);
		},

		'application/json': function () {
			res.json(error);
		}
	});
}

router.get('/', function (req, res, next) {
	exchange.create().then(x => {
		let currencies = x.getCurrencies();
		let title = 'Currency converter';
		res.format({
			'text/html': function () {
				return res.render('index', { title, docTitle: title, currencies });
			},
			'application/json': function () {
				res.json({ currencies });
			}
		});
	}).catch(next);
});

router.get('/calculate', function (req, res, next) {
	let { currencyFrom, currencyTo, amount } = req.query;

	currencyFrom = currencyFrom.toUpperCase();
	currencyTo = currencyTo.toUpperCase();
	amount = Number(amount);

	if (isNaN(amount)) {
		return formatError(res, {message: 'Invalid amount'}, 400);
	}

	exchange.create().then(x => {
		let rateFrom = x.getLatestRate(currencyFrom);
		let rateTo = x.getLatestRate(currencyTo);

		if (!rateFrom || !rateTo) {
			let error = {
				message: !rateFrom && !rateTo ?
					`Unknown currencies: ${currencyFrom}, ${currencyTo}` :
					`Unknown currency: ${currencyFrom || currencyTo}`
			};

			return formatError(res, error, 404);
		}

		let exchange = x.calculate(rateFrom, rateTo, amount);

		let result = { amount, rateFrom, rateTo, exchange };

		return res.format({
			'text/html': function () {
				return res.render('result', _.assign(result, {
					title: 'Currency converter',
					docTitle: `${amount} ${rateFrom.currency} = ${exchange} ${rateTo.currency} | Currency converter`
				}));
			},

			'application/json': function () {
				return res.json(result);
			}
		});
	}).catch(next);
});

router.get('/history/:currency', function (req, res, next) {
	let { currency } = req.params;

	exchange.create().then(x => {
		let history = x.getCurrencyHistory(currency);

		if (_.isEmpty(history)) {
			let error = {
				message: `No history found for currency ${history}`
			};

			return formatError(res, error, 404);
		}

		return res.format({
			'text/html': function () {
				return res.render('history', {
					history,
					title: `History of ${currency}`,
					docTitle: `History of ${currency}`
				});
			},
			'application/json': function () {
				return res.json({history});
			}
		});
	}).catch(next);
});

module.exports = router;
