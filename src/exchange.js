let _ = require('lodash');

let feedparser = require('./feedparser');

var getExchanges = (function () {
	var exchangeRates;

	return function (force = false) {
		if (exchangeRates && !force) {
			return Promise.resolve(exchangeRates);
		}

		return feedparser.fetch().then(x => {
			exchangeRates = x;
			return x;
		});
	};
}());


var Exchanges = (function () {
	function Exchanges (exchangeRates) {
		this.exchangeRates = exchangeRates;
	}

	Exchanges.prototype = {
		getCurrencies () {
			return _(this.exchangeRates).map('currency').uniq().value();
		},

		hasCurrency (currency) {
			return _.includes(this.getCurrencies(), currency);
		},

		getLatestRate (currency) {
			return _(this.exchangeRates).filter({currency}).last();
		},

		calculate (rateFrom, rateTo, amount = 100) {
			let amountInEur = amount / rateFrom.rate;
			return amountInEur * rateTo.rate;
		},

		getCurrencyHistory (currency) {
			currency = currency.toUpperCase();

			const pickProps = ['time', 'rate'];

			return {
				currency,
				data: _(this.exchangeRates).filter({currency}).map(x => _.pick(x, pickProps)).value()
			};
		}
	};

	return Exchanges;

}());

function create () {
	return getExchanges().then(exchangeRates => new Exchanges(exchangeRates));
}

module.exports = {
	create,
	Exchanges,
	getExchanges
};
